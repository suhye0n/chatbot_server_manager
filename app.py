import numpy as np
from flask import Flask, request, jsonify
import mysql.connector
import tensorflow as tf

app = Flask(__name__)

# MySQL 연결 설정
db = mysql.connector.connect(
    host="localhost",
    user="root",
    password="1121",
    database="chat"
)

# 토큰화 및 단어 집합 구축
def build_vocab(messages):
    tokenizer = tf.keras.preprocessing.text.Tokenizer()
    tokenizer.fit_on_texts(messages)
    return tokenizer

# 데이터베이스에서 대화 데이터 가져오기
def get_conversations():
    cursor = db.cursor(dictionary=True)
    cursor.execute("SELECT message, response FROM conversations")
    conversations = cursor.fetchall()
    cursor.close()
    return conversations

# 모델 초기화 및 데이터 전처리
def initialize_model_and_preprocess(messages):
    tokenizer = build_vocab(messages)
    vocab_size = len(tokenizer.word_index) + 1  # 0을 포함한 단어 집합의 크기

    max_len = max(len(message.split()) for message in messages)  # 가장 긴 메시지의 길이

    X = tokenizer.texts_to_sequences(messages)
    X = tf.keras.preprocessing.sequence.pad_sequences(X, maxlen=max_len, padding='post')

    return tokenizer, vocab_size, max_len, X

# 챗봇 API 엔드포인트
@app.route('/chatbot', methods=['POST'])
def chatbot():
    data = request.get_json()
    user_message = data['message']
    
    response = generate_chatbot_response(user_message)

    print(user_message)
    print(response)

    add_conversation_to_db(user_message, response)
    
    return jsonify({'response': response})

# 챗봇 응답 생성 함수
def generate_chatbot_response(message):
    best_match = find_best_match(message)
    if best_match:
        return best_match['response']
    else:
        return "죄송해요, 잘 이해하지 못했어요."

# 대화 이력에서 가장 유사한 메시지 찾기
def find_best_match(message):
    conversations = get_conversations()

    best_score = -1
    best_match = None
    
    for convo in conversations:
        response = convo['response']
        
        if response == "죄송해요, 잘 이해하지 못했어요.":
            continue
        
        score = similarity_score(message, convo['message'])
        
        if score > best_score:
            best_score = score
            best_match = convo
    
    if best_score >= 0.5 and best_match:
        return best_match
    else:
        return None

# 간단한 유사도 점수 계산 함수
def similarity_score(message1, message2):
    if message1 == message2:
        return 1.0
    else:
        return 0.0

# 대화 저장 함수
def add_conversation_to_db(message, response):
    cursor = db.cursor()
    sql = "INSERT INTO conversations (message, response) VALUES (%s, %s)"
    val = (message, response)
    cursor.execute(sql, val)
    db.commit()
    cursor.close()

if __name__ == '__main__':
    # app.run(debug=True, port=8000)
